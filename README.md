Summary: The federal government has prioritized climate change and environmental justice as core foci during the Biden Administration. This is an opportunity to build on the work from 2016-18 that EPA was doing around citizen and community science and identify structures that can support strengthened and new points of input for community data and information.

What’s the idea?
Development of three workshops in support of inclusion of community data, information and insight during environmental decision-making processes.

- Learning Agenda Development is designed for federal employees looking to integrate public and third sector data into assessments and decision making, especially with regards to climate change adaptation, migration, mitigation and environmental justice screening data.
- Needs Assessment Workshop identifies changes needed to legacy federal data systems and supports greater data dialogue between federal data systems and community-sourced data. This training is designed for technical administrators or data stewards. 
- Prototyping Exercise catalogs federal data stewards’ knowledge needs and deeply engages with a narrowly defined restriction to data integration or access. This training is geared to those responsible for maintaining data and data platforms and for those responding directly to implementation requirements and/or data pull requests. 

